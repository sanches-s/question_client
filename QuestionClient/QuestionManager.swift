//
//  QuestionManager.swift
//  QuestionClient
//
//  Created by Александр Сучков on 21.09.17.
//  Copyright © 2017 Александр Сучков. All rights reserved.
//

import Foundation
import UIKit //только для тестового создания картинок

class QuestionManager: NSObject {
    
    static let shared = QuestionManager()
    
    private override init() {
        super.init()
    }
    
    
    func autorisation(withTeam teamName: String, execute: () -> Void) {
        
        //Отправляем имя команды на сервер, в ответ получаем ее код который потом и будем использовать
        
        execute()
    }
    
    func send(answer: String) {
        
        print(answer)
    }
    
    
    func getNextQuestion() -> Question? {
    
        
        //Пока просто слуайным оразом берем из забиты вопросов, потом будем с сервера
        let randomQuestions = [
            Question(cells: [QuestionTextCell(text: "1 + 1")]),
            Question(cells: [QuestionTextCell(text: "2 + 2")]),
            Question(cells: [QuestionTextCell(text: "3 + 3")]),
            Question(cells: [QuestionTextCell(text: "Что это?"),
                             QuestionImageCell(imageData: UIImagePNGRepresentation(UIImage(named: "test1")!)!)
                ])
        
        ]
        
        let random = Int(arc4random())
        let randomInArray = random % (randomQuestions.count + 1) //+1 чтобы выйти за границы массива что вопросы закончились
        
        return randomInArray < randomQuestions.count ? randomQuestions[ randomInArray] : nil
        
    }
    
    
}


class Question: NSObject {
    
    let cells: [QuestionCell]
    
    init( cells: [QuestionCell]) {
        self.cells = cells
    }
    
    
}
