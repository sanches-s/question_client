//
//  ViewController.swift
//  QuestionClient
//
//  Created by Александр Сучков on 14.09.17.
//  Copyright © 2017 Александр Сучков. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController, UITextFieldDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        teamName.delegate = self
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        startAction(textField)
        
        return true
    }
    
    @IBOutlet weak var teamName: UITextField!
    @IBAction func startAction(_ sender: Any) {
      
        guard let textTeamName = teamName.text else {
            return
        }
        QuestionManager.shared.autorisation(withTeam: textTeamName) {
            self.performSegue(withIdentifier: "showQuestions", sender: self)
        }
    }

}

