//
//  CellClasses.swift
//  QuestionClient
//
//  Created by Александр Сучков on 22.09.17.
//  Copyright © 2017 Александр Сучков. All rights reserved.
//

import UIKit

class QuestionCell: NSObject {
    
    enum CellTypes {
        case text
        case image
    }
    
    let type: CellTypes
    
    init(type: CellTypes) {
        
        self.type = type
        super.init()
        
    }
    
    func fillCell(_ cell: UITableViewCell) {
        
        cell.textLabel?.text = "Epty cell"
        
    }
}

class QuestionTextCell: QuestionCell {
    
    let text: String
    
    init(text: String) {
        
        self.text = text
        
        super.init(type: .text)
        
    }
    
    override func fillCell(_ cell: UITableViewCell) {
        cell.textLabel?.text = self.text
    }
    
}

class QuestionImageCell: QuestionCell {
    
    let imageData: Data
    
    init(imageData: Data) {
        
        self.imageData = imageData
        super.init(type: .image)
        
    }
    
    override func fillCell(_ cell: UITableViewCell) {
        guard let imageCell = cell as? ImageCell else {
            super.fillCell(cell)
            return
        }
        
        imageCell.cellImage.image = UIImage(data: imageData)
    }
}

