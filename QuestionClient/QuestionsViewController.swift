//
//  QuestionsViewController.swift
//  QuestionClient
//
//  Created by Александр Сучков on 15.09.17.
//  Copyright © 2017 Александр Сучков. All rights reserved.
//

import UIKit

class QuestionsViewController: UIViewController, UITextViewDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    var questionCellsArray: [QuestionCell] = []
    
    let cellDictionary: [QuestionCell.CellTypes: String] = [ .text: "textCell", .image: "imageCell"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        answerView.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 50
        tableView.rowHeight = UITableViewAutomaticDimension
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        reply(answerView)
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var answerView: UITextView!
    
    @IBAction func reply(_ sender: Any) {
        
        QuestionManager.shared.send(answer: answerView.text)
        answerView.text = ""

        guard let newQuestion = QuestionManager.shared.getNextQuestion() else {
            print("Вопросов больше нет")
            
            self.performSegue(withIdentifier: "showFinish", sender: self)
            return
        }
        
        questionCellsArray = newQuestion.cells
        tableView.reloadData()
    
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        
        return true
    }
    
    
    // MARK: TableView data source metods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return questionCellsArray.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let activeQuestionCell = questionCellsArray[indexPath.row]
        
        guard let cellIdentifier = cellDictionary[activeQuestionCell.type], let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) else {
            return UITableViewCell()
        }
        
        activeQuestionCell.fillCell(cell)
        
        return cell
    }
}


class ImageCell: UITableViewCell {

    @IBOutlet weak var cellImage: UIImageView!
    
}



